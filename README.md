# Spring MVC with Jetty

A skeleton project to clone when I need a Spring MVC application running in
an embedded Jetty. Produces an executable JAR that starts Jetty on a specified
port and serves the application.

Logging is to Syslog using logback.

Freemarker is used instead of JSP, but it's simple to switch to JSP/JSTL.

Current versions are

* Jetty 9.1
* Spring 3.2.5

# Usage

Invoke from the command line like:

    java - jar webapp-1.0.0-SNAPSHOT.jar
    
Also accepts options:

* --port

* -- help
