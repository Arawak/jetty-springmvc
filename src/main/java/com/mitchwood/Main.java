package com.mitchwood;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mitchwood.web.WebServer;

public class Main {

    enum PARAMS {port, help};

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    @SuppressWarnings("static-access")
    public static void main(String... args) throws Exception {

        HelpFormatter formatter = new HelpFormatter();

        Options options = new Options();
        options.addOption(OptionBuilder.withLongOpt(PARAMS.help.name())
                .withDescription("show help")
                .withType(Number.class)
                .hasArg()
                .withArgName("help")
                .create());
        options.addOption(OptionBuilder.withLongOpt(PARAMS.port.name())
                .withDescription("port to run the application on")
                .withType(Number.class)
                .hasArg()
                .withArgName("p")
                .create());

        CommandLineParser parser = new PosixParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (UnrecognizedOptionException ex) {
            formatter.printHelp("webapp", options );
            return;
        }

        if (cmd.hasOption(PARAMS.help.name())) {
            formatter.printHelp("webapp", options );
            return;
        }

        int port = 8000;
        if (cmd.hasOption(PARAMS.port.name())) {
            port = ((Number) cmd.getParsedOptionValue(PARAMS.port.name())).intValue();
        }

        logger.info("starting application on port {}", port);
        new Main(port).start();
    }

    private WebServer server;

    public Main(int port) {
        server = new WebServer(port);
    }

    public Main() {
        server = new WebServer(8000);
    }

    public void start() throws Exception {
        server.start();
        server.join();
    }
}