package com.mitchwood.web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.RequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;


/**
 * Example WebServer class which sets up an embedded Jetty appropriately whether
 * running in an IDE or in "production" mode in a shaded jar.
 */
public class WebServer {

    PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

    private String accessLogPath = null;//"./var/logs/access/yyyy_mm_dd.request.log";
    private int minThreads = 10;
    private int maxThreads = 100;

    private static final String WEB_XML = "META-INF/webapp/WEB-INF/web.xml";
    private static final String PROJECT_RELATIVE_PATH_TO_WEBAPP = "src/main/java/META-INF/webapp";

    public static interface WebContext {
        public File getWarPath();
        public String getContextPath();
    }

    private Server server;
    private int port;
    private String bindInterface;

    public WebServer(int aPort) {
        this(aPort, null);
    }

    public WebServer(int aPort, String aBindInterface) {
        port = aPort;
        bindInterface = aBindInterface;
    }

    public void start() throws Exception {
        server = new Server(createThreadPool());
        ServerConnector conn = new ServerConnector(server);
        if (bindInterface != null) {
            conn.setHost(bindInterface);
        }
        conn.setPort(port);
        server.setConnectors(new Connector[]{conn});
        server.setHandler(createHandlers());
        server.setStopAtShutdown(true);

        server.start();
    }

    public void join() throws InterruptedException {
        server.join();
    }

    public void stop() throws Exception {
        server.stop();
    }

    private ThreadPool createThreadPool() {
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(minThreads);
        threadPool.setMaxThreads(maxThreads);
        return threadPool;
    }

    private HandlerCollection createHandlers() {
        WebAppContext ctx = new WebAppContext();
        ctx.setContextPath("/");
        String url = getContextPath();
        ctx.setWar(url);

        List<Handler> handlers = new ArrayList<Handler>();

        handlers.add(ctx);

        HandlerList contexts = new HandlerList();
        contexts.setHandlers(handlers.toArray(new Handler[0]));

        RequestLogHandler accessLog = new RequestLogHandler();
        if (accessLogPath != null) {
            accessLog.setRequestLog(createRequestLog());
        }

        HandlerCollection result = new HandlerCollection();
        result.setHandlers(new Handler[] { contexts, accessLog });

        return result;
    }

    private RequestLog createRequestLog() {
        NCSARequestLog accessLog = new NCSARequestLog();

        File file = new File(accessLogPath);
        file.getParentFile().mkdirs();

        accessLog.setFilename(file.getPath());
        accessLog.setRetainDays(90);
        accessLog.setExtended(false);
        accessLog.setAppend(true);
        accessLog.setLogTimeZone(TimeZone.getDefault().getID());
        accessLog.setLogLatency(true);
        return accessLog;
    }

    protected String getContextPath() {
        Resource r = resolver.getResource(WEB_XML);
        String str;
        try {
            str = r.getURL().toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return str.replace("WEB-INF/web.xml", "");
    }

    public void setAccessLogPath(String accessLogPath) {
        this.accessLogPath = accessLogPath;
    }

    public void setMinThreads(int minThreads) {
        this.minThreads = minThreads;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setBindInterface(String bindInterface) {
        this.bindInterface = bindInterface;
    }
}